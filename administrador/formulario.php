<?php
session_start();
if (!isset($_SESSION['userid'])){

	header("location:../login/login.php");
}
else{
	require "../conexion/conexion.php";
	include '../clases/operaciones/operaciones.php';
	require_once '../clases/pagineo/Zebra_Pagination.php';
	$operaciones = new operaciones();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Datos</title>
		<meta charset="ISO-8859-1">	
		<link rel="icon" href="../img/favicon.png" type="image/png" />
		<link rel="stylesheet" type="text/css" href="../css/estilo.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="../css/zebra_pagination.css" media="screen,print" />
		
	</head>
	<body>
	
		<div id="imHeaderBg"> </div>		
	<div id="imPage">
		  <div id="imHeader"> </div>
			
	  <div id="imMnMn" class="auto">
	  
	    <div id="usuario"> 
	  <?php	 
	  $id=$_SESSION['userid']; 
	  $operaciones->mostrar_usuario('pw_m_usuario','USU_Nombre','USU_Apellido','USU_IdUsuario', $id)
	  ?>	 
	  </div>
	  
				<ul class="auto">
					<li id="imMnMnNode0">
						<a href="../principal/inicio.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Inicio</span>
							</span>
						</a>
					</li>
					<li id="imMnMnNode4">
						<a href="../posts/misposts.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Posts</span>
							</span>
						</a>
					</li>	
					<li id="imMnMnNode6" class="imMnMnCurrent"><?php if($operaciones->administrador('pw_m_usuario','USU_IdUsuario', $id, 'USU_Usuario')==1){?>
						<a href="../administrador/formulario.php"><?php } else{?>						
						<a href="../datos/datos.php"><?php }?>
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Datos</span>
							</span>
						</a>
					</li><li id="imMnMnNode7">
						<a href="../carrito/mostrar_carrito.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Carrito</span>
							</span>
						</a>
					</li><li id="imMnMnNode5">
						<a href="../cerrar-sesion/cerrar-sesion.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Cerrar sesion</span>
							</span>
						</a>
					</li>
				</ul>
	  </div>
			<div id="imContentGraphics2">
			<br><br><div align="center"><h1><font color="#00557F">SELECCIONE MODO</font></h1><br>
				<font size="4px"><a class="enlace"  href="administra.php"><IMG SRC="../img/administrador.png"><br>ADMINISTRADOR</a></font></div>
				<br><br>
				<div align="center">
				<font size="4px"><a class="enlace" href="../datos/datos.php"><IMG SRC="../img/normal.png"><br>CUENTA PERSONAL</a></font>
				</div>
			</div>			
	</div>
		
</body>
</html>
<?php }?>