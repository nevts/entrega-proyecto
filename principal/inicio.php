<?php 
session_start();
if (!isset($_SESSION['userid'])){

	header("location:../login/login.php");
}
else{
	require "../conexion/conexion.php";
	include '../clases/operaciones/operaciones.php';	
	$operaciones = new operaciones();
	require_once '../clases/pagineo/Zebra_Pagination.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Sasu community</title>
		<meta charset="ISO-8859-1">	
		<link rel="icon" href="../img/favicon.png" type="image/png" />
		<link rel="stylesheet" type="text/css" href="../css/estilo.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="../css/zebra_pagination.css" media="screen,print" />
		
        </head>
	<body>
	<div id="imHeaderBg"> </div>		
	<div id="imPage">
		  <div id="imHeader"> </div>			
	  <div id="imMnMn" class="auto">
	  
	  <div id="usuario"> 
	  <?php	 
	  $id=$_SESSION['userid']; 
	  $operaciones->mostrar_usuario('pw_m_usuario','USU_Nombre','USU_Apellido','USU_IdUsuario', $id)
	  ?>	 
	  </div>
    
				<ul class="auto">
					<li id="imMnMnNode0" class="imMnMnCurrent">
						<a href="inicio.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Inicio</span>
							</span>
						</a>
					</li>
					<li id="imMnMnNode4">
						<a href="../posts/misposts.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Posts</span>
							</span>
						</a>
					</li>					
					<li id="imMnMnNode6">
					<?php if($operaciones->administrador('pw_m_usuario','USU_IdUsuario', $id, 'USU_Usuario')==1){?>
						<a href="../administrador/formulario.php"><?php } else{?>						
						<a href="../datos/datos.php"><?php }?>
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Datos</span>
							</span>
						</a>
					</li>
					<li id="imMnMnNode7">
						<a href="../carrito/mostrar_carrito.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Carrito</span>
							</span>
						</a>
					</li><li id="imMnMnNode5">
						<a href="../cerrar-sesion/cerrar-sesion.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Cerrar sesion</span>
							</span>
						</a>
					</li>
				</ul>
	  </div>
			<div id="imContentGraphics2">
			<div align="center"><br><br>
			<h1><font color="#00557F">ULTIMOS POSTS</font></h1>
			</div><br>
		 
			<div class="datagrid">
			<table border="2" align="center">	
			<thead><tr>			
			<th>T�TULO</th>
			<th>VISTA PREVIA</th>
			<th>PUBLICADO POR</th>
			<th>CATEGOR�A</th>	
			<th>VER</th>			
			</tr></thead>
			
			<?php
			$sql1="select count(*) cantidad from pw_t_cabarchivo"; 			
			$rec1= mysql_query($sql1);
			$results1=mysql_fetch_array($rec1);
			$total_posts=$results1['cantidad'];
			$resultados=50;			
			
			$paginacion= new Zebra_Pagination();
			$paginacion->records($total_posts);
			$paginacion->records_per_page($resultados);
			$pagina_actual=(($paginacion->get_page())-1)*$resultados;
			
			
			
			$sql = "SELECT *FROM pw_t_cabarchivo order by CAB_IdArchivo DESC limit $pagina_actual , $resultados ";
			$rec = mysql_query($sql);
			
			while($result = mysql_fetch_array($rec))
			{
				$idss=$result['CAB_IdArchivo'];	
				$portada=$result['CAB_Portada'];
				$categ=$result['CAT_IdCategoria'];
				$usu=$result['USU_IdUsuario'];
				$sql3 = "SELECT categoria.CAT_Descripcion FROM pw_m_categoria categoria ,pw_t_cabarchivo archivo 
		  		where categoria.CAT_IdCategoria=$categ";
				$rec3 = mysql_query($sql3);
				$result3 = mysql_fetch_array($rec3);
				$sql4 = "SELECT usuario.USU_Usuario FROM pw_m_usuario usuario ,pw_t_cabarchivo archivo
				where archivo.USU_IdUsuario=usuario.USU_IdUsuario and archivo.CAB_IdArchivo=$idss";
				$rec4 = mysql_query($sql4);
				$result4 = mysql_fetch_array($rec4);
			?>
			<tbody>						
			<td><?php echo $result['CAB_Titulo']?></td>
			<td><img src="<?php if($portada==""){echo "../img/previa.jpg"; }else {echo "../posts/".$portada;}?>" heigth="32px" width="32px"></td>
			<td><?php echo $result4['USU_Usuario'];?></td>
			<td><?php echo $result3['CAT_Descripcion'];?></td>	
			<td><a class="elim" href="../posts/ver.php?idss=<?php echo $idss;?>">Ver</a></td>			
			</tr></tbody>
			<?php }?>					
			</table></div><br><br>
			<?php $paginacion->labels('Anterior','Siguiente'); $paginacion->render();?>
			
	</div>
			
				</body>
</html>
<?php }?>