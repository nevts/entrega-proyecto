<?php 
session_start();
if (!isset($_SESSION['userid'])){

	header("location:../login/login.php");
}
else{
	require "../conexion/conexion.php";
	include '../clases/operaciones/operaciones.php';
	$operaciones = new operaciones();
	if(isset($_GET['error']))
	{
		echo '<script language="javascript">alert("Aseg�rese de que todos los campos esten llenos");</script>';
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Crear Post</title>
		<meta charset="ISO-8859-1">	
		<link rel="icon" href="../img/favicon.png" type="image/png" />
		<link rel="stylesheet" type="text/css" href="../css/estilo.css" media="screen,print" />
	</head>
	<body>
	
		<div id="imHeaderBg"> </div>		
	<div id="imPage">
		  <div id="imHeader"> </div>
			
	  <div id="imMnMn" class="auto">
	   
	  <div id="usuario"> 
	  <?php	 
	  $id=$_SESSION['userid']; 
	  $operaciones->mostrar_usuario('pw_m_usuario','USU_Nombre','USU_Apellido','USU_IdUsuario', $id)
	  ?>	 
	  </div>
	  
	  	<ul class="auto">
					<li id="imMnMnNode0">
						<a href="../principal/inicio.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Inicio</span>
							</span>
						</a>
					</li>
					<li id="imMnMnNode4" class="imMnMnCurrent">
						<a href="../posts/misposts.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Posts</span>
							</span>
						</a>
					</li>	
					<li id="imMnMnNode6"><?php if($operaciones->administrador('pw_m_usuario','USU_IdUsuario', $id, 'USU_Usuario')==1){?>
						<a href="../administrador/formulario.php"><?php } else{?>						
						<a href="../datos/datos.php"><?php }?>
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Datos</span>
							</span>
						</a>
					</li><li id="imMnMnNode7">
						<a href="../carrito/mostrar_carrito.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Carrito</span>
							</span>
						</a>
					</li><li id="imMnMnNode5">
						<a href="../cerrar-sesion/cerrar-sesion.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Cerrar sesion</span>
							</span>
						</a>
					</li>
				</ul>
	  </div>
	  <div id="imContentGraphics4">
	  
	  <form action="operaciones.php" method="post" enctype="multipart/form-data">
      <div id="archivo">
		      <h2><font color="#00557F">NUEVO POST</font></h2><br>
		      <div>
		 <label><font color="red">*</font>Titulo: </label> <textarea name="titulo" rows="2" cols="30"></textarea>
		 </div><br> 
		 
		       <div>
		 <label><font color="red">*</font>Descripcion: </label> <textarea name="descripcion" rows="5" maxlength="1000" cols="60"></textarea>
		 </div><br> 
		 <div>
		 <label><font color="red">*</font>Categor�a:    </label><select name="categoria">
			<?php		
				$operaciones->select_option('pw_m_categoria','CAT_IdCategoria','CAT_Descripcion');
			?>		
			</select> 
						
		 </div><br>
		  <div>
		  <p><font color="silver"><font color="#00FF00">*</font>Si su archivo es de categor�a Compra / venta agregar precio, caso contrario dejar en blanco.</font></p>
		 <label>Costo: </label><br>
		 <textarea name="costo" rows="1" cols="10"></textarea><font color="silver">0.00</font>
		 </div><br>
		 <div id="portada">
		 <p><font color="silver"><font color="#00FF00">*</font>Recuerde que luego de guardar la portada no podra ser subida, ni actualizada.</font></p>
		 <label>Portada: </label> 
		      <input type="file" id="imagen" name="archivo">
		 </div><br>
		  <p><font color="silver"><font color="#00FF00">*</font>No olvide Clickear Enter entre enlace y enlace. PSDTA:Enlace y Pass no son necesarios en Compra / Venta.</font><p>
		 <div>
		 <label>Enlaces: </label><br>
		 <textarea name="enlaces" rows="10" cols="60"></textarea>
		 </div><br>
		 <div>
		 <label>Pass: </label><br>
		 <textarea name="descomprimir" rows="1" cols="60"></textarea>
		 </div><br>		
		 <div id="guardar">
		 <input type="submit" name="guardar" value="Guardar">
		 </div>
	  </div><br> 
 </form>
 </div>
      
 </div>
 </body>
</html>
<?php }?>
 