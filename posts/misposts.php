<?php 
session_start();
if (!isset($_SESSION['userid'])){

	header("location:../login/login.php");
}
else{
	require "../conexion/conexion.php";
	include '../clases/operaciones/operaciones.php';	
	$operaciones = new operaciones();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Posts</title>
		<meta charset="ISO-8859-1">	
		<link rel="icon" href="../img/favicon.png" type="image/png" />
		<link rel="stylesheet" type="text/css" href="../css/estilo.css" media="screen,print" />
        </head>
	<body>
	<div id="imHeaderBg"> </div>		
	<div id="imPage">
		  <div id="imHeader"> </div>			
	  <div id="imMnMn" class="auto">
	  
	  <div id="usuario"> 
	  <?php	 
	  $id=$_SESSION['userid']; 
	  $operaciones->mostrar_usuario('pw_m_usuario','USU_Nombre','USU_Apellido','USU_IdUsuario', $id)
	  ?>	 
	  </div>
    
				<ul class="auto">
					<li id="imMnMnNode0">
						<a href="../principal/inicio.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Inicio</span>
							</span>
						</a>
					</li>
					<li id="imMnMnNode4" class="imMnMnCurrent">
						<a href="../posts/misposts.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Posts</span>
							</span>
						</a>
					</li>					
					<li id="imMnMnNode6">
					<?php if($operaciones->administrador('pw_m_usuario','USU_IdUsuario', $id, 'USU_Usuario')==1){?>
						<a href="../administrador/formulario.php"><?php } else{?>						
						<a href="../datos/datos.php"><?php }?>
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Datos</span>
							</span>
						</a>
					</li><li id="imMnMnNode7">
						<a href="../carrito/mostrar_carrito.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"> <span class="imMnMnImg"> </span>Carrito</span>
							</span>
						</a>
					</li><li id="imMnMnNode5">
						<a href="../cerrar-sesion/cerrar-sesion.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"> </span>Cerrar sesion</span>
							</span>
						</a>
					</li>
				</ul>
	  </div>
			<div id="imContentGraphics2"> 
			
			<br><br><div align="center"><h1><font color="#00557F">SELECCIONE ACCI�N</font></h1><br>
				<font size="4px"><a class="enlace"  href="posts.php"><IMG SRC="../img/misposts.png"><br>MIS POSTS</a></font></div>
				<br><br>
				<div align="center">
				<font size="4px"><a class="enlace" href="nuevo.php"><IMG SRC="../img/nuevopost.png"><br>NUEVO POST</a></font>
				</div>
							
			</div>
	  
	</div>
			
				</body>
</html>
<?php }?>